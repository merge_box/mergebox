# Merge Box

Merge Box is a game in work

## Important information for developers

If you programm on one of the python-editions of mergebox please go to them repos. If you want push new changes, please make a seperata repository or use your own branch before you push, and debiankaios merge if it looks good.

## Versioning

Versioning work like this: `major.minor.minimally-patch`
If minimally or patch are 0, the respective 0 and the respective dot/dash in
front of it are not written. major is a big version which change much. Minor are
little updates. minimally are minor-update which come before the minor. And
patches are bugfixes.

Examples:

| What will released | Version(Example) | Full-Version(Example) |
| -- | -- | -- |
| Major | `1.0` | `1.0.0-0` |
| Minor | `1.1` | `1.1.0-0` |
| minimally without patch | `1.1.1` | `1.1.1-0` |
| patch without minimally | `1.1-1` | `1.1.0-1` |
| minimally and patch | `1.1.1-1` | `1.1.1-1` |
