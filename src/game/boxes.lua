--[[
	This file is part of Merge Box, a game all about merging boxes to get
	boxes of higher tiers.

	Copyright (C) 2022  Merge Box authors
	Copyright (C) 2021  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

merge_box.game.selected = nil

merge_box.game.Boxtype = {
    name = "unknown",
    stage = 1,
    texture = "box_?.png",
    width = 32,
    height = 32,
}

merge_box.game.Box = {
    boxtype = merge_box.game.Box,
    area = merge_box.functions.Area:newAdjusted {
        top_left = merge_box.functions.Point:new(),
        width = 32,
        height = 32
    },
}

function merge_box.game.Boxtype:new(o)
    o = o or {}
    setmetatable(o, {__index = self})
    o.drawable = love.graphics.newImage("data/img/"..o.texture)
    return o
end

function merge_box.game.Box:new(o)
    o = o or {}
    setmetatable(o, {__index = self})
    return o
end

function merge_box.game.Box:create(o)
    o.area = merge_box.functions.Area:newAdjusted{
        top_left = o.point,
        width = o.boxtype.width,
        height = o.boxtype.height
    }
    o.dragging_state = {}
    return self:new(o)
end

function merge_box.game.Box:draw()
    merge_box.graphics.draw(self.boxtype.drawable, self.area:left(), self.area:top(), 0, self.boxtype.width/self.boxtype.drawable:getPixelWidth(), self.boxtype.height/self.boxtype.drawable:getPixelHeight())
end

function merge_box.game.Box:update()
    if merge_box.events.mouse_pressed_in_area(self.area, 1) and not merge_box.game.selected then
        self.dragging_state.mouse_start_point = merge_box.events.mouse_position()
        self.dragging_state.box_start_point = self.area.top_left
        merge_box.game.selected = self.dragging_state
    end
    if self:selected() then
        self.area.top_left = merge_box.functions.Point:new{x=self.dragging_state.box_start_point.x+merge_box.events.mouse_position().x-self.dragging_state.mouse_start_point.x, y=self.dragging_state.box_start_point.y+merge_box.events.mouse_position().y-self.dragging_state.mouse_start_point.y}
        if not merge_box.events.mouse_pressed_in_area(self.area, 1) then
            merge_box.game.selected = nil
        end
    end
end

function merge_box.game.Box:change_boxtype(boxtype)
    self.boxtype = boxtype
end

function merge_box.game.Box:merge_box(mergedata)
    self.boxtype = mergedata.boxtype or self.boxtype
end

function merge_box.game.Box:selected()
    if self.dragging_state == merge_box.game.selected then
        return true
    else
        return false
    end
end
