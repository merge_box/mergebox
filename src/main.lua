--[[
	This file is part of Merge Box, a game all about merging boxes to get
	boxes of higher tiers.

	Copyright (C) 2022  Merge Box authors
	Copyright (C) 2021  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

function love.quit()
    if merge_box.mode == 0 then
        merge_box.functions.save_menu()
    elseif merge_box.mode == 1 then
        merge_box.functions.save_game("infinity_mode")
    else
        -- Shouldn't happen; print warning
        print("Warning: Couldn't save game: Unknown subgame: " .. merge_box.mode)
    end
    return false
end

function love.load()
    love.graphics.setBackgroundColor(0.30078125, 0.30078125, 0.30078125)
    if not merge_box.functions.isdir(merge_box.save_dir) then
        merge_box.functions.create_directory(merge_box.save_dir)
    end
    love.graphics.setNewFont("data/fonts/LiberationSans-Regular.ttf"):setFilter("nearest", "nearest")
    local f = io.open(merge_box.functions.join_path(merge_box.save_dir, "menu.json"),"r")
    if f then
        local table = json.decode(f:read("*all")) or {}
        merge_box.menu.selected_profile = tonumber(table["selected_profile"]) or 1
        merge_box.menu.selected_mode = tonumber(table["selected_mode"]) or 1
        merge_box.menu.username = table["username"] or {text = ""}
        f:close()
    end
    merge_box.chat.send("Warning: Infinity Mode is in testing-phase and story mode not aviable now", "menu")
end

function love.update(dt)
    merge_box.test.update()
    if merge_box.mode == 0 then
        menu.update(dt)
    elseif merge_box.mode == 1 then
        merge_box.games.infinity_mode.update(dt)
    end
end

function love.draw()
    merge_box.test.draw()
    if merge_box.mode == 0 then
        menu.draw()
        merge_box.chat.draw()
    elseif merge_box.mode == 1 then
        merge_box.games.infinity_mode.draw()
    end
    suit.draw()
end

function love.textinput(t)
	suit.textinput(t)
end

function love.keypressed(key)
	suit.keypressed(key)
end
