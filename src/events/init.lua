--[[
	This file is part of Merge Box, a game all about merging boxes to get
	boxes of higher tiers.

	Copyright (C) 2022  Merge Box authors
	Copyright (C) 2021  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

require "src/functions"

function merge_box.events.mouse_position()
    return merge_box.functions.Point:new{
        x = love.mouse.getX(),
        y = love.mouse.getY()
    }
end

function merge_box.events.mouse_pressed_in_area(area, mouse_button)
    if area:contains(merge_box.events.mouse_position()) and love.mouse.isDown(mouse_button) then
        return true
    else
        return false
    end
end
