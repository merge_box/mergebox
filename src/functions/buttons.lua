--[[
	This file is part of Merge Box, a game all about merging boxes to get
	boxes of higher tiers.

	Copyright (C) 2022  Merge Box authors
	Copyright (C) 2021  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

merge_box.functions.Button = {
    x = 0,
    y = 0,
    sx = 1,
    sy = 1,
    width = 60,
    height = 20,
    text = "",
    touched_color = {1,1,1},
    touched_mode = "fill",
    touched_textcolor = {0,0,0},
    nottouched_color = {0,0,0},
    nottouched_mode = "fill",
    nottouched_textcolor = {1,1,1},
}

merge_box.functions.ImageButton = {
    x = 0,
    y = 0,
    width = 60,
    height = 20,
    touched_texture = "",
    nottouched_texture = "",
}

function merge_box.functions.Button:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    return o
end

function merge_box.functions.Button:draw()
    local x = self.x
    local y = self.y
    local sx = self.sx
    local sy = self.sy
    local width = self.width
    local height = self.height
    local text = self.text
    local area = merge_box.functions.Area:newAdjusted{
        top_left = merge_box.functions.Point:new{x=x,y=y},
        width = width,
        height = height
    }
    if area:contains(merge_box.events.mouse_position()) then
        merge_box.graphics.rectangle({
            mode = self.touched_mode,
            area = area,
            color = self.touched_color,
        })
        love.graphics.setColor(self.touched_textcolor)
        love.graphics.print(text, x+(width-love.graphics.getFont():getWidth(text)*sx)/2, y+(height-love.graphics.getFont():getHeight(text)*sy)/2, 0, sx, sy)
        love.graphics.setColor(1,1,1)
    else
        merge_box.graphics.rectangle({
            mode = self.nottouched_mode,
            area = area,
            color = self.nottouched_color,
        })
        love.graphics.setColor(self.nottouched_textcolor)
        love.graphics.print(text, x+(width-love.graphics.getFont():getWidth(text)*sx)/2, y+(height-love.graphics.getFont():getHeight(text)*sy)/2, 0, sx, sy)
        love.graphics.setColor(1,1,1)
    end
end

function merge_box.functions.Button:is_pressed()
    local x = self.x
    local y = self.y
    local width = self.width
    local height = self.height
    local area = merge_box.functions.Area:newAdjusted{
        top_left = merge_box.functions.Point:new{x=x,y=y},
        width = width,
        height = height
    }
    return merge_box.events.mouse_pressed_in_area(area, 1)
end

function merge_box.functions.ImageButton:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    return o
end

function merge_box.functions.ImageButton:draw()
    local x = self.x
    local y = self.y
    local width = self.width
    local height = self.height
    local path_to_textures = "data/img/"
    local area = merge_box.functions.Area:newAdjusted{
        top_left = merge_box.functions.Point:new{x=x,y=y},
        width = width,
        height = height
    }
    if area:contains(merge_box.events.mouse_position()) then
        merge_box.graphics.image_in_area(love.graphics.newImage(path_to_textures..self.touched_texture), area)
    else
        merge_box.graphics.image_in_area(love.graphics.newImage(path_to_textures..self.nottouched_texture), area)
    end
end

function merge_box.functions.ImageButton:is_pressed()
    local x = self.x
    local y = self.y
    local width = self.width
    local height = self.height
    local area = merge_box.functions.Area:newAdjusted{
        top_left = merge_box.functions.Point:new{x=x,y=y},
        width = width,
        height = height
    }
    return merge_box.events.mouse_pressed_in_area(area, 1)
end
