--[[
	This file is part of Merge Box, a game all about merging boxes to get
	boxes of higher tiers.

	Copyright (C) 2022  Merge Box authors
	Copyright (C) 2021  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

merge_box.functions.Area = {
    top_left = merge_box.functions.Point:new{x=0,y=0},
    width = 10,
    height = 10
}

function merge_box.functions.Area:new(o)
    o = o or {}
    setmetatable(o, {__index = self})
    return o
end

function merge_box.functions.Area:from_to(top_left, bottom_right)
    local width = bottom_right.x - top_left.x
    local height = bottom_right.y - top_left.y
    return self:new{
        top_left = top_left,
        width = width,
        height = height
    }
end

function merge_box.functions.Area:left()
    return self.top_left.x
end

function merge_box.functions.Area:right()
    return self:left() + self.width
end

function merge_box.functions.Area:top()
    return self.top_left.y
end

function merge_box.functions.Area:bottom()
    return self:top() + self.height
end

function merge_box.functions.Area:center_x()
    return self.top_left.x + self.width/2
end

function merge_box.functions.Area:center_y()
    return self.top_left.y + self.height/2
end

function merge_box.functions.Area:center()
    return merge_box.functions.Point:new{
        x = self:center_x(),
        y = self:center_y()
    }
end

function merge_box.functions.Area:top_right()
    return merge_box.functions.Point:new{
        x = self:right(),
        y = self:top()
    }
end

function merge_box.functions.Area:bottom_left()
    return merge_box.functions.Point:new{
        x = self:left(),
        y = self:bottom()
    }
end

function merge_box.functions.Area:bottom_right()
    return merge_box.functions.Point:new{
        x = self:right(),
        y = self:bottom()
    }
end

function merge_box.functions.Area:contains(point)
    return (
        point.x >= self:left() and
        point.x <= self:right() and
        point.y >= self:top() and
        point.y <= self:bottom()
    )
end

function merge_box.functions.Area:has_intersection(area)
    return (
        self:left() <= area:right() and
        self:right() >= area:left() and
        self:top() <= area:bottom() and
        self:bottom() >= area:top()
    )
end
