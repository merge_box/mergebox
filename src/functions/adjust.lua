--[[
	This file is part of Merge Box, a game all about merging boxes to get
	boxes of higher tiers.

	Copyright (C) 2022  Merge Box authors
	Copyright (C) 2021  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

merge_box.functions.default_display_area = merge_box.functions.Area:new{
    top_left = merge_box.functions.Point:new{
        x = 0,
        y = 0
    },
    width = 800,
    height = 600
}

function merge_box.functions.adjustedPoint(p)
    local width, height, flags = love.window.getMode()
    return merge_box.functions.Point:new{
        x = (p.x / merge_box.functions.default_display_area.width ) * width,
        y = (p.y / merge_box.functions.default_display_area.height) * height
    }
end

function merge_box.functions.adjustX(x)
    return merge_box.functions.adjustedPoint(merge_box.functions.Point:new{x = x, y = 0}).x
end

function merge_box.functions.adjustY(y)
    return merge_box.functions.adjustedPoint(merge_box.functions.Point:new{x = 0, y = y}).y
end


function merge_box.functions.adjustedArea(area)
    local top_left     = merge_box.functions.adjustedPoint(area.top_left)
    local bottom_right = merge_box.functions.adjustedPoint(area:bottom_right())
    return merge_box.functions.Area:from_to(top_left, bottom_right)
end

function merge_box.functions.Point:newAdjusted(o)
    o = o or {}
    setmetatable(o, {__index = self})
    o.x=merge_box.functions.adjustX(o.x)
    o.y=merge_box.functions.adjustY(o.y)
    return o
end

function merge_box.functions.Area:newAdjusted(o)
    o = o or {}
    setmetatable(o, {__index = self})
    o.top_left=merge_box.functions.adjustedPoint(o.top_left)
    o.width=merge_box.functions.adjustX(o.width)
    o.height=merge_box.functions.adjustY(o.height)
    return o
end
