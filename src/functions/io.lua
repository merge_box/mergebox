--[[
	This file is part of Merge Box, a game all about merging boxes to get
	boxes of higher tiers.

	Copyright (C) 2022  Merge Box authors
	Copyright (C) 2021  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

function merge_box.functions.join_path(part1, part2)
    return(part1.."/"..part2)
end

local function join_path(part1, part2)
    return merge_box.functions.join_path(part1, part2)
end

function merge_box.functions.exists(file)
    local ok, err, code = os.rename(file, file)
    if not ok then
        if code == 13 then
            -- Permission denied, but it exists
            return true
        end
    end
    return ok, err
end

function merge_box.functions.isdir(path)
    return merge_box.functions.exists(merge_box.functions.join_path(path, ""))
end

function merge_box.functions.create_directory(path)
    if love.system.getOS() == "Windows" then -- Windows isn't working at the moment
        os.execute('mkdir "' .. path .. '"')
    else
        os.execute('mkdir -p "' .. path .. '"')
    end
end

function merge_box.functions.save_file(path, content)
    local f = io.open(path,"w")
    f:write(content)
    f:close()
end

function merge_box.functions.save_json_file(path, table)
    merge_box.functions.save_file(path, json.encode(table))
end

function merge_box.functions.load_file(path)
    local f = io.open(path,"r")
    local content = nil
    if f then
        content = f:read("*all")
        f:close()
    end
    return content
end

function merge_box.functions.load_json_file(path)
    local content = merge_box.functions.load_file(path)
    if content then
        content = json.decode(content)
    end
    return content
end

function merge_box.functions.save_menu()
    merge_box.functions.save_json_file(merge_box.functions.join_path(merge_box.save_dir, "menu.json"),merge_box.menu)
end

function merge_box.functions.save_game(gamename)
    local profilepath = "profile"..tostring(merge_box.menu.selected_profile)
    if not merge_box.functions.isdir(join_path(merge_box.save_dir,profilepath)) then
        merge_box.functions.create_directory(join_path(merge_box.save_dir,profilepath))
    end
    merge_box.functions.save_json_file(join_path(join_path(merge_box.save_dir,profilepath),tostring(gamename)..".json"), merge_box.games[gamename].tosave)
end

function merge_box.functions.load_game(gamename)
    local profilepath = "profile"..tostring(merge_box.menu.selected_profile)
    merge_box.games[gamename].tosave = merge_box.functions.load_json_file(join_path(join_path(merge_box.save_dir,profilepath),gamename..".json")) or {}
end
