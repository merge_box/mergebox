--[[
	This file is part of Merge Box, a game all about merging boxes to get
	boxes of higher tiers.

	Copyright (C) 2022  Merge Box authors
	Copyright (C) 2021  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

merge_box.functions.math = {}

function merge_box.functions.math.round(number)
    return math.floor(number+0.5)
end

function merge_box.functions.math.random(m, n)
    math.randomseed(os.time())
    return math.random(m, n)
end
