--[[
	This file is part of Merge Box, a game all about merging boxes to get
	boxes of higher tiers.

	Copyright (C) 2022  Merge Box authors
	Copyright (C) 2021  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

merge_box.functions.suffix = {
    suffix = "",
    value = 1,
    roundat = nil,
}

function merge_box.functions.suffix:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    return o
end

function merge_box.functions.suffix:round_to_factor()
    return 10^self.roundat
end

function merge_box.functions.suffix:use(number)
    if self.roundat then
        return(tostring(merge_box.functions.math.round(number/self.value*self:round_to_factor())/(self:round_to_factor()))..self.suffix)
    else
        return(tostring(number/self.value..self.suffix))
    end
end

function merge_box.functions.suffix:extract(numberwithsuffix)
    return(tonumber(numberwithsuffix:sub(1,numberwithsuffix:len()-self.suffix:len()))*self.value)
end

merge_box.functions.suffix_cluster = {
    suffixes = {{suffix=merge_box.functions.suffix:new(), min=nil}}
}

function merge_box.functions.suffix_cluster:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    return o
end

function merge_box.functions.suffix_cluster:BestSuffix(number)
    local function compare_greater_nil_smaller(v1, v2)
        if v1 == nil and v2 == nil then
            return false
        elseif v1 == nil then
            return false
        elseif v2 == nil then
            return true
        else
            return v1 > v2
        end
    end

    local bestsuffix = nil
    for i, suffix in pairs(self.suffixes) do
        if (
            (not bestsuffix)
            or (
                compare_greater_nil_smaller(suffix.min, bestsuffix.min)
                and compare_greater_nil_smaller(number, suffix.min)
            )
        ) then
            bestsuffix = suffix
        end
    end
    return bestsuffix.suffix
end

function merge_box.functions.suffix_cluster:useBestSuffix(number)
    return self:BestSuffix(number):use(number)
end
