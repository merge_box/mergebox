--[[
	This file is part of Merge Box, a game all about merging boxes to get
	boxes of higher tiers.

	Copyright (C) 2022  Merge Box authors
	Copyright (C) 2021  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

require "src/functions"

menu = {}
local menudata = {username = {text = ""}}

-- TODO: Use four parameters instead of layout
-- See also https://suit.readthedocs.io/en/latest/gettingstarted.html#hello-world

local function inputAt(area, variable)
    local adjusted_area = merge_box.functions.adjustedArea(area)
    suit.layout:reset(adjusted_area.top_left.x, adjusted_area.top_left.y)
    return suit.Input(variable, suit.layout:row(adjusted_area.width, adjusted_area.height))
end

menu.start_button = merge_box.functions.Button:new({
    x = 5,
    y = 90,
    width = 130,
    height = 30,
    text = "Start Game!",
    touched_color = {0.3,0.5,0.7},
    touched_mode = "fill",
    touched_textcolor = {0,0,0},
    nottouched_color = {0.5,0.5,0.5},
    nottouched_mode = "fill",
    nottouched_textcolor = {0,0,0},
})

function menu.update(dt)
    -- Ctrl-S
    if love.keyboard.isDown("lctrl") and love.keyboard.isDown("s") then
        if not merge_box.cache.save then
            merge_box.functions.save_menu()
            merge_box.cache.save = true
            merge_box.chat.send("Menu data were saved", "menu")
        end
    else
        if merge_box.cache.save then merge_box.cache.save = false end
    end

    -- Username input
    inputAt(
        merge_box.functions.Area:new{
            top_left = merge_box.functions.Point:new{
	x = 5,
	y = 40
            },
            width = 130,
            height = 30
        },
        merge_box.menu.username
    )

    --{{{Start game button
    if menu.start_button:is_pressed() then
        --{{{Start game
        merge_box.functions.save_menu()
        if merge_box.menu.selected_mode == 1 then
            merge_box.mode = merge_box.menu.selected_mode
            merge_box.functions.load_game("infinity_mode")
            require "games/infinity_mode/maingame/init"
            if merge_box.menu.selected_mode == 1 then
                merge_box.game.path = "games/infinity_mode"
            end
        else
            merge_box.chat.send("This mode doesn't exist or isn't aviable now", "menu")
        end
        --}}}
    end
    --}}}

    -- Start Profiles
    if merge_box.events.mouse_pressed_in_area(merge_box.functions.Area:from_to(merge_box.functions.Point:new{x=680,y=5}, merge_box.functions.Point:new{x=696,y=21}), 1) then
        merge_box.menu.selected_profile = 1
    elseif merge_box.events.mouse_pressed_in_area(merge_box.functions.Area:from_to(merge_box.functions.Point:new{x=680,y=35}, merge_box.functions.Point:new{x=696,y=51}), 1) then
        merge_box.menu.selected_profile = 2
    elseif merge_box.events.mouse_pressed_in_area(merge_box.functions.Area:from_to(merge_box.functions.Point:new{x=680,y=65}, merge_box.functions.Point:new{x=696,y=81}), 1) then
        merge_box.menu.selected_profile = 3
    elseif merge_box.events.mouse_pressed_in_area(merge_box.functions.Area:from_to(merge_box.functions.Point:new{x=680,y=95}, merge_box.functions.Point:new{x=696,y=111}), 1) then
        merge_box.menu.selected_profile = 4
    elseif merge_box.events.mouse_pressed_in_area(merge_box.functions.Area:from_to(merge_box.functions.Point:new{x=680,y=125}, merge_box.functions.Point:new{x=696,y=141}), 1) then
        merge_box.menu.selected_profile = 5
    elseif merge_box.events.mouse_pressed_in_area(merge_box.functions.Area:from_to(merge_box.functions.Point:new{x=680,y=155}, merge_box.functions.Point:new{x=696,y=171}), 1) then
        merge_box.menu.selected_profile = 6
    end
    -- End Profiles

    -- Start Modes
    if merge_box.events.mouse_pressed_in_area(merge_box.functions.Area:from_to(merge_box.functions.Point:new{x=5,y=145}, merge_box.functions.Point:new{x=21,y=161}), 1) then
        merge_box.menu.selected_mode = 1
    elseif merge_box.events.mouse_pressed_in_area(merge_box.functions.Area:from_to(merge_box.functions.Point:new{x=5,y=175}, merge_box.functions.Point:new{x=21,y=191}), 1) then
        merge_box.menu.selected_mode = 2
    end
    -- End Modes

    suit.layout:row()
end

function menu.draw()
    merge_box.graphics.print("Username:", 5, 0, 0, 2, 2)
    --Buttons
    menu.start_button:draw()
    -- Start Modes
    merge_box.graphics.print("Infinity Mode", 25, 140, 0, 2, 2)
    merge_box.graphics.draw(merge_box.graphics.checkbox, 5, 145, 0)
    merge_box.graphics.print("Story Mode", 25, 170, 0, 2, 2)
    merge_box.graphics.draw(merge_box.graphics.checkbox, 5, 175, 0)
    merge_box.graphics.draw(merge_box.graphics.red_x, 5, (merge_box.menu.selected_mode-1)*30+145, 0)
    -- End Modes

    -- Start Profiles
    merge_box.graphics.print("Profile 1", 700, 0, 0, 2, 2)
    merge_box.graphics.draw(merge_box.graphics.checkbox, 680, 5, 0)
    merge_box.graphics.print("Profile 2", 700, 30, 0, 2, 2)
    merge_box.graphics.draw(merge_box.graphics.checkbox, 680, 35, 0)
    merge_box.graphics.print("Profile 3", 700, 60, 0, 2, 2)
    merge_box.graphics.draw(merge_box.graphics.checkbox, 680, 65, 0)
    merge_box.graphics.print("Profile 4", 700, 90, 0, 2, 2)
    merge_box.graphics.draw(merge_box.graphics.checkbox, 680, 95, 0)
    merge_box.graphics.print("Profile 5", 700, 120, 0, 2, 2)
    merge_box.graphics.draw(merge_box.graphics.checkbox, 680, 125, 0)
    merge_box.graphics.print("Profile 6", 700, 150, 0, 2, 2)
    merge_box.graphics.draw(merge_box.graphics.checkbox, 680, 155, 0)
    merge_box.graphics.draw(merge_box.graphics.red_x, 680, (merge_box.menu.selected_profile-1)*30+5, 0)
    -- End Profiles
end
