--[[
	This file is part of Merge Box, a game all about merging boxes to get
	boxes of higher tiers.

	Copyright (C) 2022  Merge Box authors
	Copyright (C) 2021  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

local menu_chat_msg = {"","","","","","",}

function merge_box.chat.send(message, where, player)
    if where == "menu" then
        table.insert(menu_chat_msg, message)
    end
end

function merge_box.chat.draw()
    merge_box.graphics.print(menu_chat_msg[#menu_chat_msg], 0, 390, 0, 2, 2)
    merge_box.graphics.print(menu_chat_msg[#menu_chat_msg-1], 0, 420, 0, 2, 2)
    merge_box.graphics.print(menu_chat_msg[#menu_chat_msg-2], 0, 450, 0, 2, 2)
    merge_box.graphics.print(menu_chat_msg[#menu_chat_msg-3], 0, 480, 0, 2, 2)
    merge_box.graphics.print(menu_chat_msg[#menu_chat_msg-4], 0, 510, 0, 2, 2)
    merge_box.graphics.print(menu_chat_msg[#menu_chat_msg-5], 0, 540, 0, 2, 2)
end
