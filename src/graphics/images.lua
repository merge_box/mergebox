--[[
	This file is part of Merge Box, a game all about merging boxes to get
	boxes of higher tiers.

	Copyright (C) 2022  Merge Box authors
	Copyright (C) 2021  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

love.graphics.setDefaultFilter("linear", "linear")

love.graphics.setDefaultFilter("nearest", "nearest")
merge_box.graphics.checkbox = love.graphics.newImage("data/img/checkbox_empty_white.png")
merge_box.graphics.red_x = love.graphics.newImage("data/img/symbol_x_red.png")
merge_box.graphics.trash_can = love.graphics.newImage("data/img/trash_can.png")
merge_box.graphics.shop_button = love.graphics.newImage("data/img/shop_button.png")
merge_box.graphics.coin = love.graphics.newImage("data/img/coin.png")
