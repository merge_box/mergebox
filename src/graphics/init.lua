--[[
	This file is part of Merge Box, a game all about merging boxes to get
	boxes of higher tiers.

	Copyright (C) 2022  Merge Box authors
	Copyright (C) 2021  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

-- TODO: Work with points & areas

local function adjustX(x)
    return merge_box.functions.adjustX(x)
end

local function adjustY(y)
    return merge_box.functions.adjustY(y)
end

function merge_box.graphics.draw(drawable, x, y, r, sx, sy, ox, oy, kx, ky)
    local x = x or 0
    local y = y or 0
    local r = r or 0
    local sx = sx or 1
    local sy = sy or sx
    local ox = ox or 0
    local oy = oy or 0
    local kx = kx or 0
    local ky = ky or 0
    love.graphics.draw(drawable, adjustX(x), adjustY(y), r, adjustX(sx), adjustY(sy), adjustX(ox), adjustY(oy), adjustX(kx), adjustY(ky))
end

function merge_box.graphics.print(text, x, y, r, sx, sy, ox, oy, kx, ky)
    local x = x or 0
    local y = y or 0
    local r = r or 0
    local sx = sx or 1
    local sy = sy or sx
    local ox = ox or 0
    local oy = oy or 0
    local kx = kx or 0
    local ky = ky or 0
    love.graphics.print(text, adjustX(x), adjustY(y), r, adjustX(sx), adjustY(sy), adjustX(ox), adjustY(oy), adjustX(kx), adjustY(ky))
end

function merge_box.graphics.rectangle(def)
    local color = def.color or {1,1,1}
    local mode = def.mode or "fill"
    local area = def.area
    local rx = def.rx or 1
    local ry = def.ry or rx
    love.graphics.setColor(color)
    love.graphics.rectangle(mode, adjustX(area:left()), adjustY(area:top()), adjustX(area.width), adjustY(area.height), adjustX(rx), adjustY(ry), def.segments)
    love.graphics.setColor(1,1,1)
end

function merge_box.graphics.image_in_area(drawable, area)
    love.graphics.draw(drawable, area:left(), area:top(), 0, area.width/drawable:getWidth(), area.height/drawable:getHeight())
end

function merge_box.graphics.image_in_area_adjusted(drawable, area)
    merge_box.graphics.draw(drawable, area:left(), area:top(), 0, area.width/drawable:getWidth(), area.height/drawable:getHeight())
end

require "src/graphics/images"
require "src/graphics/chat"
