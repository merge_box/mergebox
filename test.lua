#!/bin/lua

--[[
	This file is part of Merge Box, a game all about merging boxes to get
	boxes of higher tiers.

	Copyright (C) 2022  Merge Box authors
	Copyright (C) 2021  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

local luaunit = require("lib/luaunit/luaunit")

local system = "linux"

love = {
    system = {
        getOS = function()
            return system
        end
    }
}

-- Initialize empty tables because merge_box can't be required without love
-- Note for future development: Only do anything in a function. The file should only define values but not call any function.
merge_box = {
    functions = {}
}

require "src/functions/init"

testRound = function()
    luaunit.assertEquals(merge_box.functions.math.round(2), 2)
    luaunit.assertEquals(merge_box.functions.math.round(2.3), 2)
    luaunit.assertEquals(merge_box.functions.math.round(2.5), 3)
    luaunit.assertEquals(merge_box.functions.math.round(1.8), 2)
    luaunit.assertEquals(merge_box.functions.math.round(8.5), 9)
end

testJoinPath = function()
    system = "Windows"
    luaunit.assertEquals(merge_box.functions.join_path("a", "b"), "a/b")
    luaunit.assertEquals(merge_box.functions.join_path("a", "b"), "a/b")
    luaunit.assertEquals(merge_box.functions.join_path("a", "b/c"), "a/b/c")
    luaunit.assertEquals(merge_box.functions.join_path("a/b", ""), "a/b/")
    system = "linux"
    luaunit.assertEquals(merge_box.functions.join_path("a", "b"), "a/b")
    luaunit.assertEquals(merge_box.functions.join_path("a", "b/c"), "a/b/c")
    luaunit.assertEquals(merge_box.functions.join_path("a/b", ""), "a/b/")
end

os.exit(luaunit.LuaUnit.run())
