--[[
	This file is part of Merge Box, a game all about merging boxes to get
	boxes of higher tiers.

	Copyright (C) 2022  Merge Box authors
	Copyright (C) 2021  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

json = require "lib/json/json"
suit = require "lib/suit"

merge_box = {}
merge_box.boxes = {}
merge_box.boxes_on_field = {}
merge_box.cache = {}
merge_box.cache.save = false
merge_box.chat = {}
merge_box.events = {}
merge_box.functions = {}
merge_box.game = {}
merge_box.game.path = ""
merge_box.games = {}
merge_box.games.infinity_mode = {}
merge_box.graphics = {}
merge_box.menu = {}
merge_box.mode = 0
merge_box.menu.selected_mode = 1
merge_box.menu.selected_profile = 1
merge_box.menu.username = {text = ""}
merge_box.test = {}
merge_box.version = "0.2.1"
merge_box.protocol_version = 5
merge_box.menu.last_version_launched = {version=merge_box.version,protocol_version=merge_box.protocol_version}
if love.system.getOS() ~= "Windows" then
    merge_box.save_dir = love.filesystem.getAppdataDirectory().."merge_box"
else
    merge_box.save_dir = love.filesystem.getAppdataDirectory().."\\merge_box"
end

require "src"
require "games"
