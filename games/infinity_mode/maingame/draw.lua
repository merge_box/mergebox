--[[
	This file is part of Merge Box, a game all about merging boxes to get
	boxes of higher tiers.

	Copyright (C) 2022  Merge Box authors
	Copyright (C) 2021  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

local function printmoney(money)
    merge_box.graphics.print(money, 140, 549, 0, 3, 3)
end

function merge_box.games.infinity_mode.draw()
    merge_box.graphics.rectangle{
        color = {0.2, 0.2, 0.2},
        area = merge_box.functions.Area:new{
            top_left = merge_box.functions.Point:new{x=0,y=536},
            width = 800,
            height = 64
        }
    }
    merge_box.graphics.draw(merge_box.graphics.trash_can, 5, 544, 0, 4, 3)
    merge_box.games.infinity_mode.shop_button:draw()
    merge_box.graphics.draw(merge_box.graphics.coin, 100, 552, 0, 0.5, 0.5)
    printmoney(merge_box.games.infinity_mode.suffix_cluster:useBestSuffix(merge_box.games.infinity_mode.tosave.money))
    for i, j in ipairs(merge_box.games.infinity_mode.existing_boxes) do
        j:draw()
    end
    if merge_box.games.infinity_mode.shop.shop_open then
        merge_box.games.infinity_mode.shop.draw_shop()
    end
end
