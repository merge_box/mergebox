--[[
	This file is part of Merge Box, a game all about merging boxes to get
	boxes of higher tiers.

	Copyright (C) 2022  Merge Box authors
	Copyright (C) 2021  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

merge_box.games.infinity_mode.suffixes = {}

merge_box.games.infinity_mode.suffixes.single = merge_box.functions.suffix:new{
    suffix = "",
    value = 1,
    roundat = nil,
}

merge_box.games.infinity_mode.suffixes.kilo = merge_box.functions.suffix:new{
    suffix = "k",
    value = 1000,
    roundat = 1,
}

merge_box.games.infinity_mode.suffixes.Mega = merge_box.functions.suffix:new{
    suffix = "M",
    value = 1000000,
    roundat = 1,
}

merge_box.games.infinity_mode.suffixes.Giga = merge_box.functions.suffix:new{
    suffix = "G",
    value = 1000000000,
    roundat = 1,
}

merge_box.games.infinity_mode.suffixes.Tera = merge_box.functions.suffix:new{
    suffix = "T",
    value = 1000000000000,
    roundat = 1,
}

merge_box.games.infinity_mode.suffixes.Peta = merge_box.functions.suffix:new{
    suffix = "P",
    value = 1000000000000000,
    roundat = 1,
}

merge_box.games.infinity_mode.suffixes.Exa = merge_box.functions.suffix:new{
    suffix = "E",
    value = 1000000000000000000,
    roundat = 1,
}

merge_box.games.infinity_mode.suffixes.Zetta = merge_box.functions.suffix:new{
    suffix = "Z",
    value = 1000000000000000000000,
    roundat = 1,
}

merge_box.games.infinity_mode.suffixes.Yotta = merge_box.functions.suffix:new{
    suffix = "Y",
    value = 1000000000000000000000000,
    roundat = 1,
}

merge_box.games.infinity_mode.suffix_cluster = merge_box.functions.suffix_cluster:new{
    suffixes = {
        {suffix=merge_box.games.infinity_mode.suffixes.single, min=nil},
        {suffix=merge_box.games.infinity_mode.suffixes.kilo, min=1000},
        {suffix=merge_box.games.infinity_mode.suffixes.Mega, min=1000000},
        {suffix=merge_box.games.infinity_mode.suffixes.Giga, min=1000000000},
        {suffix=merge_box.games.infinity_mode.suffixes.Tera, min=1000000000000},
        {suffix=merge_box.games.infinity_mode.suffixes.Peta, min=1000000000000000},
        {suffix=merge_box.games.infinity_mode.suffixes.Exa, min=1000000000000000000},
        {suffix=merge_box.games.infinity_mode.suffixes.Zetta, min=1000000000000000000000},
        {suffix=merge_box.games.infinity_mode.suffixes.Yotta, min=1000000000000000000000000}
    }
}
