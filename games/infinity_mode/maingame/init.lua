--[[
	This file is part of Merge Box, a game all about merging boxes to get
	boxes of higher tiers.

	Copyright (C) 2022  Merge Box authors
	Copyright (C) 2021  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

merge_box.games.infinity_mode.tosave = merge_box.games.infinity_mode.tosave or {}
merge_box.games.infinity_mode.tosave.last_version_launched = {version=merge_box.version,protocol_version=merge_box.protocol_version}

if not merge_box.games.infinity_mode.tosave.money then
    merge_box.games.infinity_mode.tosave.money = 0
end
merge_box.games.infinity_mode.tosave.boxtimer = merge_box.games.infinity_mode.tosave.boxtimer or 0
merge_box.games.infinity_mode.tosave.time_for_new_box = merge_box.games.infinity_mode.tosave.time_for_new_box or 3
merge_box.games.infinity_mode.tosave.price_for_faster_spawning = merge_box.games.infinity_mode.tosave.price_for_faster_spawning or 100

require "games/infinity_mode/maingame/suffixes"
require "games/infinity_mode/maingame/boxes"
require "games/infinity_mode/maingame/shop"

merge_box.games.infinity_mode.shop_button = merge_box.functions.ImageButton:new{
    x = 672,
    y = 544,
    width = 128,
    height = 48,
    touched_texture = "shop_button_touched.png",
    nottouched_texture = "shop_button.png",
}

merge_box.games.infinity_mode.trash_can_area = merge_box.functions.Area:new{
    top_left = merge_box.functions.Point:new{x=21,y=551},
    width = 32,
    height = 42
}
