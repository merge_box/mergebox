--[[
	This file is part of Merge Box, a game all about merging boxes to get
	boxes of higher tiers.

	Copyright (C) 2022  Merge Box authors
	Copyright (C) 2021  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

merge_box.games.infinity_mode.shop = {}
merge_box.games.infinity_mode.shop_pages = {}
merge_box.games.infinity_mode.selected_page = 1
merge_box.games.infinity_mode.mouse_down = false
merge_box.games.infinity_mode.shop.shop_open = false

merge_box.games.infinity_mode.shop.page_button_width = 64
merge_box.games.infinity_mode.shop.page_button_height = 64
merge_box.games.infinity_mode.shop.offers_per_column = 10
merge_box.games.infinity_mode.shop.offer_area_width = 200
merge_box.games.infinity_mode.shop.offer_area_height = 40

local canvas_offers_point = merge_box.functions.Point:newAdjusted{x=10,y=100}

local function column_of_offer(item_in_offer_list)
    return math.floor((item_in_offer_list-1) / merge_box.games.infinity_mode.shop.offers_per_column)
end
local function row_of_offer(item_in_offer_list)
    return (item_in_offer_list-1) % merge_box.games.infinity_mode.shop.offers_per_column
end
local function calculate_top_left_point_of_an_item(item)
    return merge_box.functions.Point:new{
    x = column_of_offer(item) * merge_box.games.infinity_mode.shop.offer_area_width,
    y = row_of_offer(item) * merge_box.games.infinity_mode.shop.offer_area_height}
end

function merge_box.games.infinity_mode.shop.register_shop_page(shoplist)
    shoplist.nottouched_texture = shoplist.nottouched_texture or "box_?.png"
    shoplist.touched_texture = shoplist.touched_texture or shoplist.nottouched_texture
    table.insert(merge_box.games.infinity_mode.shop_pages, {
        main_button = merge_box.functions.ImageButton:new{
            x = merge_box.games.infinity_mode.shop.page_button_width*#merge_box.games.infinity_mode.shop_pages,
            y = 0,
            width = merge_box.games.infinity_mode.shop.page_button_width,
            height = merge_box.games.infinity_mode.shop.page_button_height,
            touched_texture = shoplist.main_button.touched_texture,
            nottouched_texture = shoplist.main_button.nottouched_texture,
        },
        offers = shoplist.offers
    })
end

function merge_box.games.infinity_mode.shop.generate_box_buy_offer(boxname)
    return {name=boxname,logo=merge_box.games.infinity_mode.box[boxname].texture,price=merge_box.games.infinity_mode.box[boxname].buyprice,action=function() merge_box.games.infinity_mode.create_box_at_random_spot(merge_box.games.infinity_mode.box[boxname]) end}
end

merge_box.games.infinity_mode.shop.register_shop_page({
    main_button = {touched_texture = "icon_touched.png", nottouched_texture = "icon.png",},
    offers = {
        merge_box.games.infinity_mode.shop.generate_box_buy_offer("Ones"),
        merge_box.games.infinity_mode.shop.generate_box_buy_offer("Twos"),
        merge_box.games.infinity_mode.shop.generate_box_buy_offer("Warner"),
        merge_box.games.infinity_mode.shop.generate_box_buy_offer("Corner"),
        merge_box.games.infinity_mode.shop.generate_box_buy_offer("Fives"),
        merge_box.games.infinity_mode.shop.generate_box_buy_offer("As"),
        merge_box.games.infinity_mode.shop.generate_box_buy_offer("Bs"),
        merge_box.games.infinity_mode.shop.generate_box_buy_offer("Eights"),
        merge_box.games.infinity_mode.shop.generate_box_buy_offer("Treses"),
        merge_box.games.infinity_mode.shop.generate_box_buy_offer("Hourglasses"),
        merge_box.games.infinity_mode.shop.generate_box_buy_offer("Pluses"),
        merge_box.games.infinity_mode.shop.generate_box_buy_offer("Circles"),
        merge_box.games.infinity_mode.shop.generate_box_buy_offer("Ds"),
        merge_box.games.infinity_mode.shop.generate_box_buy_offer("Es"),
        merge_box.games.infinity_mode.shop.generate_box_buy_offer("Fs"),
        merge_box.games.infinity_mode.shop.generate_box_buy_offer("Sixteenpoints"),
        merge_box.games.infinity_mode.shop.generate_box_buy_offer("Line_Corners"),
        merge_box.games.infinity_mode.shop.generate_box_buy_offer("Corner_Corners"),
        merge_box.games.infinity_mode.shop.generate_box_buy_offer("Potassium"),
        merge_box.games.infinity_mode.shop.generate_box_buy_offer("Rhombuses"),
    }
})

-- Page will be finished soon
merge_box.games.infinity_mode.shop.register_shop_page({
    main_button = {touched_texture = "shop_button_upgrade_touched.png", nottouched_texture = "shop_button_upgrade.png",},
    offers = {
        {name="Spawn Higher",logo="box_upgrade.png",price=merge_box.games.infinity_mode.box[merge_box.games.infinity_mode.tosave.spawn_stage_name].buyprice*10,action=function() merge_box.games.infinity_mode.tosave.time_for_new_box=merge_box.games.infinity_mode.tosave.time_for_new_box+(merge_box.games.infinity_mode.box[merge_box.games.infinity_mode.tosave.spawn_stage_name].stage/10)^2 merge_box.games.infinity_mode.tosave.spawn_stage_name=merge_box.games.infinity_mode.tonewboxname(merge_box.games.infinity_mode.tosave.spawn_stage_name) return merge_box.games.infinity_mode.box[merge_box.games.infinity_mode.tosave.spawn_stage_name].buyprice*10 end},
        {name="-1 sec spawn-\n time",logo="clock.png",price=merge_box.games.infinity_mode.tosave.price_for_faster_spawning,
        action=function()
            merge_box.games.infinity_mode.tosave.time_for_new_box=merge_box.games.infinity_mode.tosave.time_for_new_box-1
            merge_box.games.infinity_mode.tosave.price_for_faster_spawning=merge_box.games.infinity_mode.tosave.price_for_faster_spawning*2
            return merge_box.games.infinity_mode.tosave.price_for_faster_spawning end,
        condition=function() return(merge_box.games.infinity_mode.tosave.time_for_new_box>=3) end,},
    }
})

merge_box.games.infinity_mode.shop.exit_button = merge_box.functions.ImageButton:new{
    x = 737,
    y = 1,
    width = 62,
    height = 62,
    touched_texture = "symbol_x_red.png",
    nottouched_texture = "symbol_x_red.png",
}

for i, page in pairs(merge_box.games.infinity_mode.shop_pages) do
    for j, offer in pairs(page.offers) do
        -- The offers are arranged column wise (the whole first column first, then the second and so on)
        local top_left_point = calculate_top_left_point_of_an_item(j)
        offer.buybutton = merge_box.functions.Button:new{
            x = top_left_point.x+119+canvas_offers_point.x,
            y = top_left_point.y+canvas_offers_point.y+16,
            width = 80,
            height = 15,
            text = "buy!",
            touched_color = {0.38823529411764707, 0.6078431372549019, 1},
            touched_mode = "fill",
            touched_textcolor = {0,0,0},
            nottouched_color = {0.3568627450980392, 0.43137254901960786, 0.8823529411764706},
            nottouched_mode = "fill",
            nottouched_textcolor = {0,0,0},
        }
    end
end

function merge_box.games.infinity_mode.shop.update_shop(dt)
    if merge_box.games.infinity_mode.shop.exit_button:is_pressed() then
        merge_box.games.infinity_mode.shop.shop_open = false
    end
    if merge_box.games.infinity_mode.mouse_down and not love.mouse.isDown(1) then
        merge_box.games.infinity_mode.mouse_down = false
    end
    for i, offer in pairs(merge_box.games.infinity_mode.shop_pages[merge_box.games.infinity_mode.selected_page].offers) do
        if offer.buybutton:is_pressed() and not merge_box.games.infinity_mode.mouse_down and merge_box.games.infinity_mode.tosave.money >= offer.price and (offer.condition==nil or offer.condition()) then
            merge_box.games.infinity_mode.mouse_down = true
            merge_box.games.infinity_mode.tosave.money = merge_box.games.infinity_mode.tosave.money - offer.price
            local new_price, new_logo, new_name = offer.action()
            if new_price then
                offer.price = new_price
            end
            if new_logo then
                offer.logo = new_logo
            end
            if new_name then
                offer.name = new_name
            end
        end
    end
end

function merge_box.games.infinity_mode.shop.draw_shop()
    local canvas_shopbuttons = love.graphics.newCanvas(merge_box.functions.adjustX(736),merge_box.functions.adjustY(64))
    local canvas_offers = love.graphics.newCanvas(merge_box.functions.adjustX(800),merge_box.functions.adjustY(500))
    love.graphics.setCanvas(canvas_shopbuttons)
    for i, page in pairs(merge_box.games.infinity_mode.shop_pages) do
        page.main_button:draw()
        if page.main_button:is_pressed() then
            merge_box.games.infinity_mode.selected_page = i
        end
    end
    love.graphics.setCanvas()
    love.graphics.setColor(1,1,1)
    love.graphics.setCanvas(canvas_offers)
    for i, offer in pairs(merge_box.games.infinity_mode.shop_pages[merge_box.games.infinity_mode.selected_page].offers) do
        love.graphics.translate(canvas_offers_point.x, canvas_offers_point.y)
        local top_left_point = calculate_top_left_point_of_an_item(i)
        merge_box.graphics.rectangle{color={1,1,1}, area=merge_box.functions.Area:new{top_left=top_left_point, width=merge_box.games.infinity_mode.shop.offer_area_width, height=merge_box.games.infinity_mode.shop.offer_area_height}}
        merge_box.graphics.rectangle{color={0,0,0}, area=merge_box.functions.Area:new{top_left=top_left_point, width=merge_box.games.infinity_mode.shop.offer_area_width, height=merge_box.games.infinity_mode.shop.offer_area_height}, mode="line"}
        merge_box.graphics.rectangle{color={0.5294117647058824, 0.38823529411764707, 0.6078431372549019}, area=merge_box.functions.Area:new{top_left=merge_box.functions.Point:new{x=top_left_point.x+119, y=top_left_point.y+1}, width=80, height=15}}
        merge_box.graphics.print("Price: "..merge_box.games.infinity_mode.suffix_cluster:useBestSuffix(offer.price), top_left_point.x+119, top_left_point.y+1)
        love.graphics.setColor(0,0,0)
        merge_box.graphics.print(offer.name, top_left_point.x+40, top_left_point.y+4)
        love.graphics.setColor(1,1,1)
        merge_box.graphics.image_in_area_adjusted(love.graphics.newImage(merge_box.functions.join_path(merge_box.functions.join_path("data", "img"),offer.logo)), merge_box.functions.Area:newAdjusted{top_left = merge_box.functions.Point:new{x=top_left_point.x+4, y=top_left_point.y+4}, width=32, height=32})
        love.graphics.translate(-canvas_offers_point.x,-canvas_offers_point.y)
        offer.buybutton:draw()
    end
    love.graphics.setCanvas()
    love.graphics.setColor(1,1,1)
    love.graphics.draw(canvas_shopbuttons,0,0)
    love.graphics.draw(canvas_offers,0,0)
    merge_box.graphics.rectangle{color={1,1,1}, area=merge_box.functions.Area:new{top_left=merge_box.functions.Point:new{x=736, y=0}, width=64, height=64}}
    merge_box.games.infinity_mode.shop.exit_button:draw()
end
