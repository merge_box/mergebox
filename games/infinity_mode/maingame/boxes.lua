--[[
	This file is part of Merge Box, a game all about merging boxes to get
	boxes of higher tiers.

	Copyright (C) 2022  Merge Box authors
	Copyright (C) 2021  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

-- WARNING: Don't remove the table merge_box.games.infinity_mode.box or the "[]" in the update.lua or draw.lua. They are needed for the save system to time.
-- WARNING: Prices aren't finally. Sellprice is the money you get if you do the box in trash and the buyprice get a use soon.

merge_box.games.infinity_mode.box = {}

merge_box.games.infinity_mode.box.Ones = merge_box.game.Boxtype:new{
    name = "Ones",
    stage = 1,
    texture = "box_1.png",
    buyprice = 2^2,
    sellprice = 2^0,
}

merge_box.games.infinity_mode.box.Twos = merge_box.game.Boxtype:new{
    name = "Twos",
    stage = 2,
    texture = "box_2.png",
    buyprice = 2^3,
    sellprice = 2^1,
}

merge_box.games.infinity_mode.box.Warner = merge_box.game.Boxtype:new{
    name = "Warner",
    stage = 3,
    texture = "box_3.png",
    buyprice = 2^4,
    sellprice = 2^2,
}

merge_box.games.infinity_mode.box.Corner = merge_box.game.Boxtype:new{
    name = "Corner",
    stage = 4,
    texture = "box_4.png",
    buyprice = 2^5,
    sellprice = 2^3,
}

merge_box.games.infinity_mode.box.Fives = merge_box.game.Boxtype:new{
    name = "Fives",
    stage = 5,
    texture = "box_5.png",
    buyprice = 2^6,
    sellprice = 2^4,
}

merge_box.games.infinity_mode.box.As = merge_box.game.Boxtype:new{
    name = "As",
    stage = 6,
    texture = "box_a.png",
    buyprice = 2^7,
    sellprice = 2^5,
}

merge_box.games.infinity_mode.box.Bs = merge_box.game.Boxtype:new{
    name = "Bs",
    stage = 7,
    texture = "box_b.png",
    buyprice = 2^8,
    sellprice = 2^6,
}

merge_box.games.infinity_mode.box.Eights = merge_box.game.Boxtype:new{
    name = "Eights",
    stage = 8,
    texture = "box_8.png",
    buyprice = 2^9,
    sellprice = 2^7,
}

merge_box.games.infinity_mode.box.Treses = merge_box.game.Boxtype:new{
    name = "Treses",
    stage = 9,
    texture = "box_9.png",
    buyprice = 2^10,
    sellprice = 2^8,
}

merge_box.games.infinity_mode.box.Hourglasses = merge_box.game.Boxtype:new{
    name = "Hourglasses",
    stage = 10,
    texture = "box_x.png",
    buyprice = 2^11,
    sellprice = 2^9,
}

merge_box.games.infinity_mode.box.Pluses = merge_box.game.Boxtype:new{
    name = "Pluses",
    stage = 11,
    texture = "box_11.png",
    buyprice = 2^12,
    sellprice = 2^10,
}

merge_box.games.infinity_mode.box.Circles = merge_box.game.Boxtype:new{
    name = "Circles",
    stage = 12,
    texture = "box_12.png",
    buyprice = 2^13,
    sellprice = 2^11,
}

merge_box.games.infinity_mode.box.Ds = merge_box.game.Boxtype:new{
    name = "Ds",
    stage = 13,
    texture = "box_d.png",
    buyprice = 2^14,
    sellprice = 2^12,
}

merge_box.games.infinity_mode.box.Es = merge_box.game.Boxtype:new{
    name = "Es",
    stage = 14,
    texture = "box_e.png",
    buyprice = 2^15,
    sellprice = 2^13,
}

merge_box.games.infinity_mode.box.Fs = merge_box.game.Boxtype:new{
    name = "Fs",
    stage = 15,
    texture = "box_f.png",
    buyprice = 2^16,
    sellprice = 2^14,
}

merge_box.games.infinity_mode.box.Sixteenpoints = merge_box.game.Boxtype:new{
    name = "Sixteenpoints",
    stage = 16,
    texture = "box_16.png",
    buyprice = 2^17,
    sellprice = 2^15,
}

merge_box.games.infinity_mode.box.Line_Corners = merge_box.game.Boxtype:new{
    name = "Line_Corners",
    stage = 17,
    texture = "box_17.png",
    buyprice = 2^18,
    sellprice = 2^16,
}

merge_box.games.infinity_mode.box.Corner_Corners = merge_box.game.Boxtype:new{
    name = "Corner_Corners",
    stage = 18,
    texture = "box_18.png",
    buyprice = 2^19,
    sellprice = 2^17,
}

merge_box.games.infinity_mode.box.Potassium = merge_box.game.Boxtype:new{
    name = "Potassium",
    stage = 19,
    texture = "box_19.png",
    buyprice = 2^20,
    sellprice = 2^18,
}

merge_box.games.infinity_mode.box.Rhombuses = merge_box.game.Boxtype:new{
    name = "Rhombuses",
    stage = 20,
    texture = "box_20.png",
    buyprice = 2^21,
    sellprice = 2^19,
}

-- TODO: More boxes

merge_box.games.infinity_mode.merges = {
    ["Ones"] = "Twos",
    ["Twos"] = "Warner",
    ["Warner"] = "Corner",
    ["Corner"] = "Fives",
    ["Fives"] = "As",
    ["As"] = "Bs",
    ["Bs"] = "Eights",
    ["Eights"] = "Treses",
    ["Treses"] = "Hourglasses",
    ["Hourglasses"] = "Pluses",
    ["Pluses"] = "Circles",
    ["Circles"] = "Ds",
    ["Ds"] = "Es",
    ["Es"] = "Fs",
    ["Fs"] = "Sixteenpoints",
    ["Sixteenpoints"] = "Line_Corners",
    ["Line_Corners"] = "Corner_Corners",
    ["Corner_Corners"] = "Potassium",
    ["Potassium"] = "Rhombuses",
    ["Rhombuses"] = nil,
    ["Compressed_Rhombuses"] = nil,
    --TODO: ["Not defined Yet"] = "Titanium",
    ["Titanium"] = "Octagons",
    ["Octagons"] = nil,
}

function merge_box.games.infinity_mode.tonewboxname(name)
    return merge_box.games.infinity_mode.merges[name]
end

merge_box.games.infinity_mode.existing_boxes = {}

function merge_box.games.infinity_mode.create_box(boxtype, point)
    table.insert(
    merge_box.games.infinity_mode.existing_boxes,
    merge_box.game.Box:create{
        boxtype = boxtype,
        point = point,
    }
    )
end

function merge_box.games.infinity_mode.create_box_at_random_spot(boxtype)
    merge_box.games.infinity_mode.create_box(boxtype, merge_box.functions.Point:new{x = merge_box.functions.math.random(0, 768), y = merge_box.functions.math.random(0, 518)})
end

if merge_box.games.infinity_mode.tosave.boxes then
    for i, box in pairs(merge_box.games.infinity_mode.tosave.boxes) do
        table.insert(
        merge_box.games.infinity_mode.existing_boxes,
        merge_box.game.Box:create{
            boxtype = merge_box.games.infinity_mode.box[box.name],
            point = merge_box.functions.Point:new{x = box.top_left_point.x, y = box.top_left_point.y}
        }
    )
    end
end
merge_box.games.infinity_mode.tosave.spawn_stage_name = merge_box.games.infinity_mode.tosave.spawn_stage_name or "Ones"
