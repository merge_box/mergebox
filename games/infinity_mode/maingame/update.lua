--[[
	This file is part of Merge Box, a game all about merging boxes to get
	boxes of higher tiers.

	Copyright (C) 2022  Merge Box authors
	Copyright (C) 2021  debiankaios

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

function merge_box.games.infinity_mode.update(dt)
    merge_box.games.infinity_mode.tosave.boxtimer = merge_box.games.infinity_mode.tosave.boxtimer + dt
    if merge_box.games.infinity_mode.tosave.boxtimer >= merge_box.games.infinity_mode.tosave.time_for_new_box then
        merge_box.games.infinity_mode.create_box_at_random_spot(merge_box.games.infinity_mode.box[merge_box.games.infinity_mode.tosave.spawn_stage_name])
        merge_box.games.infinity_mode.tosave.boxtimer = 0
    end
    local isselectedboxexisting = nil --Needed if a box will delted in selected mode.
    if not merge_box.games.infinity_mode.shop.shop_open then
        merge_box.games.infinity_mode.tosave.boxes = {}
        for i, box in pairs(merge_box.games.infinity_mode.existing_boxes) do
            for j, box2 in pairs(merge_box.games.infinity_mode.existing_boxes) do
                if box.area:has_intersection(box2.area) and not (i == j) and not box:selected() and box2:selected() and box.boxtype == box2.boxtype then
                    box.other_box_dragging_state = box2.dragging_state
                elseif box.area:has_intersection(box2.area) and not (i == j) and not box:selected() and not box2:selected() and box.other_box_dragging_state == box2.dragging_state and not (merge_box.games.infinity_mode.tonewboxname(box.boxtype.name) == nil) then
                    box:merge_box{boxtype = merge_box.games.infinity_mode.box[merge_box.games.infinity_mode.tonewboxname(box.boxtype.name)]}
                    --merge_box.games.infinity_mode.tosave.money = merge_box.games.infinity_mode.tosave.money+box.boxtype.sellprice
                    table.remove(merge_box.games.infinity_mode.existing_boxes, j)
                end
            end
            box:update()
            if box.area:has_intersection(merge_box.games.infinity_mode.trash_can_area) and not box:selected() then
                merge_box.games.infinity_mode.tosave.money = merge_box.games.infinity_mode.tosave.money+box.boxtype.sellprice
                table.remove(merge_box.games.infinity_mode.existing_boxes, i)
            end
            if box:selected() then
                isselectedboxexisting = true
            end
            table.insert(merge_box.games.infinity_mode.tosave.boxes, {
                name = box.boxtype.name,
                top_left_point = {x = box.area.top_left.x, y = box.area.top_left.y},
            })
        end
    else
        merge_box.games.infinity_mode.shop.update_shop(dt)
    end

    if merge_box.games.infinity_mode.shop_button:is_pressed() and not merge_box.game.selected then
        merge_box.games.infinity_mode.shop.shop_open = true
    end
    if not isselectedboxexisting then
        merge_box.game.selected = nil
    end
end
