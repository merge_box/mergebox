# Installing Merge Box

Note: This assumes no packaging tool. If you use one, you maybe want to create a package for it.


## Packages

### .deb(debian, ubuntu, etc.)
If you use debina-similar distruption you can use the .deb of major-builds. First install requirements:
On a typical Debian-based Linux distribution, you can achieve this by running `apt install love`

After that download the `merge_box-<version>.deb` and do:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~sh
# dpkg -i merge_box-<version>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

### .exe(Windows)

Easily click on the exe-files on windows.

## Build

### Linux/MacOS

The installation process on linux is simple.

Requirements for installation:
- `install`
- `make` (supporting nested variables, e.g. GNU make)
- `rm`
- `sed`
- `zip`
By default it is assumed all of these are in /usr/bin.

For installation, just type `make install`. If you install it to use it with user before you compile with root, sudo or doas run `make install` before you run for example `sudo make install`

In case you have an unusual path to one of the commands listed above: Just list them at the make command like this:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~sh
make INSTALL=/path/to/install ZIP=/path/to/zip install
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you want to install using a different prefix, run `make PREFIX=/usr` (if your prefix is `/usr`).
Some other directory variables also are being used:
- `exec_prefix` (= prefix)
- `bindir` (= exec\_prefix/bin)
- `data_prefix` (= prefix)
- `datadir` (= data\_prefix/share)
- `applications_datadir` (= datadir/applications)
- `pkgdatadir` (= datadir/mergebox)

In case you need to install it temporarily to a different location for later moving it to your prefix, DESTDIR is being supported:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~sh
make DESTDIR=/tmp/installation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you want to see what commands are being run, add V=1:

~~~~~~~~~~~~~~~~~~sh
make V=1 install
~~~~~~~~~~~~~~~~~~

All of these can be combined:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~sh
make ZIP=/bin/zip prefix=/usr DESTDIR=/tmp/installation V=1 install
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you should need to uninstall Merge Box, simply type `make uninstall` using the same other arguments.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~sh
make ZIP=/bin/zip prefix=/usr DESTDIR=/tmp/installation V=1 uninstall
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Windows

Currently, the windows installation is something to be done. However, LÖVE provides some general instructions. See [https://love2d.org/wiki/Game_Distribution](https://love2d.org/wiki/Game_Distribution). You can use the exes of major-versions.
